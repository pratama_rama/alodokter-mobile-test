//
//  BorderedView.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 18/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class BorderedView: UIView {
  
  override func awakeFromNib() {
    layer.borderWidth = 1
    layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
    layer.masksToBounds = true
    layer.cornerRadius = 3
  }
}
