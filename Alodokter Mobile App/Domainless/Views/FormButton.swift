//
//  FormButton.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class FormButton: UIButton {
  
  override func awakeFromNib() {
    layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor
    layer.borderWidth = 1
    layer.cornerRadius = 3
    layer.masksToBounds = true
  }
}
