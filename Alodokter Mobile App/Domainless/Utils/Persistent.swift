//
//  Persistent.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import Foundation

enum PersistentKey: String, StorageTypeProtocol {
  case isLogin   = "isLogin"
  case email = "email"
  
  var storageType: PersistentStorageType {
    get {
      return .userDefault
    }
  }
}

protocol StorageTypeProtocol {
  var storageType: PersistentStorageType { get }
}

extension PersistentKey {
  private func getPrefix() -> String {
    return "alodokterMobileApp_"
  }
  
  func modifiedKey() -> String {
    return getPrefix() + self.rawValue
  }
}

enum PersistentStorageType {
  case userDefault
}

struct Persistent {
  static let shared = Persistent()
  
  func set(key: PersistentKey, value: String) {
    UserDefaults.set(value, forKey: key.modifiedKey())
  }
  
  func get(key: PersistentKey) -> String? {
    return UserDefaults.get(key.modifiedKey())
  }
  
  func delete(key: PersistentKey) {
    UserDefaults.delete(key.modifiedKey())
  }
}
