//
//  Validation.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

enum ValidationType {
  case notEmpty
  case email
  case sameTo(value: String?, name: String)
  case minimumLength(count: Int)
  case maximumLength(count: Int)
  case checked
  case minimumValue(value: Int)
  case maximumValue(value: Int)
  case trueValue(message: String, value: Bool)
  case nonNil(message: String, value: Any?)
  case minimumAge(value: Int)
  
  func toMessage(name: String?) -> String {
    let prefix = name ?? ""
    switch self {
    case .notEmpty:
      return "Mohon isi " + prefix
    case .email:
      return "Format " + prefix + " salah"
    case .sameTo(_, let theName):
      return prefix + " belum sama dengan " + theName
    case .minimumLength(let count):
      return "Panjang " + prefix + " minimal \(count) huruf"
    case .maximumLength(let count):
      return "Panjang " + prefix + " maksimal \(count) huruf"
    case .checked:
      return "Mohon pilih " + prefix
    case .minimumValue(let value):
      return prefix + " minimal \(value)"
    case .maximumValue(let value):
      return prefix + " maksimal \(value)"
    case .trueValue(let message, _),
         .nonNil(let message, _):
      return message
    case .minimumAge(let value):
      return "Anda harus berusia \(value) tahun keatas untuk menggunakan aplikasi ini"
    }
  }
  
  func evaluate(input: String?) -> Bool {
    guard let theInput = input else { return false }
    
    switch self {
    case .notEmpty:
      return !theInput.isEmpty
    case .email:
      return theInput.isValidEmailAddress
    case .sameTo(let value, _):
      return value == input
    case .minimumLength(let count):
      return (input?.count ?? 0) >= count
    case .maximumLength(let count):
      return (input?.count ?? 0) <= count
    case .checked:
      return input != nil
    case .minimumValue(let value):
      return Int(theInput) ?? 0 >= value
    case .maximumValue(let value):
      return Int(theInput) ?? 0 <= value
    case .trueValue:
      return Bool(theInput) ?? false
    case .nonNil:
      return true
    case .minimumAge(let value):
      let age = 0
      print("umur \(age)")
      return age >= value
    }
  }
}

struct ValidationInput {
  var type: [ValidationType]
  var name: String?
  weak var control: UIView?
}

enum ValidationResult {
  case valid
  case invalid(message: String, field: UIView?)
}

typealias ValidationInputs = [ValidationInput]

struct Validation {
  static func validate(inputs: ValidationInputs) -> ValidationResult {
    for input in inputs {
      for validationType in input.type.enumerated() {
        if case .checked = validationType.element {
          if !(input.control?.value(forKey: "isSelected") as? Bool ?? false) {
            let message = validationType.element.toMessage(name: input.name)
            return .invalid(message: message, field: input.control)
          }
        } else if case .trueValue(_, let val) = validationType.element {
          let message = validationType.element.toMessage(name: input.name)
          if !val { return .invalid(message: message, field: input.control) }
        }  else if case .nonNil(_, let val) = validationType.element {
          let message = validationType.element.toMessage(name: input.name)
          if val == nil { return .invalid(message: message, field: input.control) }
        } else if !validationType.element.evaluate(input: input.control?.value(forKey: "text") as? String) {
          let message = validationType.element.toMessage(name: input.name)
          return .invalid(message: message, field: input.control)
        }
      }
    }
    
    return .valid
  }
}
