//
//  UIAlertController.swift
//

import UIKit

extension UIAlertController {
  
  func show() {
    present(animated: true) {
      // TODO: solve layout issue
    }
  }
  
  func present(animated: Bool, completion: (() -> Void)?) {
    if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
      presentFromController(controller: rootVC, animated: animated, completion: completion)
    }
  }
  
  private func presentFromController(controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
    if  let navVC = controller as? UINavigationController,
      let visibleVC = navVC.visibleViewController {
      presentFromController(controller: visibleVC, animated: animated, completion: completion)
    } else {
      if  let tabVC = controller as? UITabBarController,
        let selectedVC = tabVC.selectedViewController {
        presentFromController(controller: selectedVC, animated: animated, completion: completion)
      } else {
        if controller is UIAlertController {
          return
        }
        
        controller.present(self, animated: animated, completion: completion)
      }
    }
  }
}

extension UIAlertController {
  /**
   Present an Alert Controller with custom title, message, and a single action button to dismiss the alert.
   
   - parameter title:            custom alert title
   - parameter message:          custom alert message
   - parameter buttonTitle:      custom caption for the action button
   - parameter dismissalHandler: dismissal completion handler
   */
  static func showAlert(withTitle title: String? = nil, message: String, buttonTitle: String = "OK", dismissalHandler: ((UIAlertAction) -> Void)? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let actionButton = UIAlertAction(title: buttonTitle, style: .default, handler: dismissalHandler)
    alert.addAction(actionButton)
    
    alert.show()
  }
  
  /**
   Present an Alert Controller with custom title, message, a dismissal button on the left, and another action button on the right to execute a custom handler.
   
   - parameter title:            custom alert title
   - parameter message:          custom alert message
   - parameter leftButtonTitle:  custom caption for the left action button
   - parameter rightButtonTitle: custom caption for the right action button
   - parameter nextAction:       dismissal completion handler for the right action button
   */
  static func showTwoButtonAlert(withTitle title: String? = nil, message: String, leftButtonTitle: String, rightButtonTitle: String, nextAction: @escaping (UIAlertAction) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let leftActionButton = UIAlertAction(title: leftButtonTitle, style: .cancel) {
      _ in
      alert.dismiss(animated: true, completion: nil)
    }
    let rightActionButton = UIAlertAction(title: rightButtonTitle, style: .default, handler: nextAction)
    
    alert.addAction(leftActionButton)
    alert.addAction(rightActionButton)
    
    alert.show()
  }
  
  static func showTwoButtonAlert(withTitle title: String, message: String, leftButtonTitle: String, rightButtonTitle: String,
                                 leftAction: @escaping (UIAlertAction) -> Void, rightAction: @escaping (UIAlertAction) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let leftActionButton = UIAlertAction(title: leftButtonTitle, style: .cancel, handler: leftAction)
    let rightActionButton = UIAlertAction(title: rightButtonTitle, style: .default, handler: rightAction)
    
    alert.addAction(leftActionButton)
    alert.addAction(rightActionButton)
    
    alert.show()
  }
}
