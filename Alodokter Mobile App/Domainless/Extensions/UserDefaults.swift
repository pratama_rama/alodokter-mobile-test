//
//  UserDefaults.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import Foundation

extension UserDefaults {
  static func get(_ key: String) -> String? {
    let val = UserDefaults.standard.value(forKey: key)
    if let v = val as? String {
      return v
    }
    
    return nil
  }
  
  static func delete(_ key: String) {
    UserDefaults.standard.removeObject(forKey: key)
    UserDefaults.standard.synchronize()
  }
  
  static func set(_ val: String, forKey key: String) {
    UserDefaults.standard.set(val, forKey: key)
    UserDefaults.standard.synchronize()
  }
}
