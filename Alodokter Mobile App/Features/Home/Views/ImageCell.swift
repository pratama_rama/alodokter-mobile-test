//
//  ImageCell.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 18/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
  @IBOutlet private weak var imgItem: UIImageView!
  
  var item: UIImage? {
    didSet {
      imgItem.image = item
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}

