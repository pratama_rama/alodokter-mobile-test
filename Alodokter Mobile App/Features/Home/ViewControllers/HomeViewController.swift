//
//  HomeViewController.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  @IBOutlet private weak var table: UITableView!
  
  private let imagesAll: [[UIImage]] = [[UIImage(named: "captainamerica")!, UIImage(named: "captainamerica2")!, UIImage(named: "captainamerica3")!], [UIImage(named: "ironman")!, UIImage(named: "ironman2")!, UIImage(named: "ironman3")!],[UIImage(named: "spiderman")!, UIImage(named: "spiderman2")!, UIImage(named: "spiderman3")!]]
  
  private let images: [UIImage] = [UIImage(named: "captainamerica")!, UIImage(named: "ironman")!, UIImage(named: "spiderman")!]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  private func setupUI() {
    navigationItem.title = "My Apps"
    table.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
    table.delegate = self
    table.dataSource = self
    table.rowHeight = UITableView.automaticDimension
    table.estimatedRowHeight = 44
    navigationController?.interactivePopGestureRecognizer?.isEnabled = true
  }
}

extension HomeViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    let selectedItems = imagesAll[indexPath.row]
    let detail = DetailImageViewController(images: selectedItems)
    navigationController?.pushViewController(detail, animated: true)
  }
}

extension HomeViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return images.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
    
    cell.item = images[indexPath.row]
    
    return cell
  }
  
  
}
