//
//  HomeTabBarViewController.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

enum HomeTabType {
  case home
  case profile
  
  var item: HomeTabItem {
    switch self {
    case .home:
      return HomeTabItem(title: "Home", icon: UIImage(named: "home"))
    case .profile:
      return HomeTabItem(title: "Profile", icon: UIImage(named: "profile"))
    }
  }
  
  var viewController: UIViewController {
    switch self {
    case .home:
      let home = HomeViewController(nibName: "HomeViewController", bundle: nil)
      return AHKNavigationController(rootViewController: home)
    case .profile:
      let profile = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
      return AHKNavigationController(rootViewController: profile)
    }
  }
}

struct HomeTabItem {
  var title: String?
  var icon: UIImage?
}

final class HomeTabBarViewController: UITabBarController {
  private let tabs: [HomeTabType] = [.home, .profile]
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    
    setupViewControllers()
    setupTabBar()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupViewControllers() {
    var viewControllers = [UIViewController]()
    for tab in tabs {
      let viewController = tab.viewController
      viewController.tabBarItem.image = tab.item.icon
      viewController.tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -10, right: 0)
      viewController.tabBarItem.title = nil
      viewController.tabBarItem.setTitleTextAttributes(
        [.font: UIFont.systemFont(ofSize: 0)],
        for: .normal)
      viewController.tabBarItem.setTitleTextAttributes(
        [.font: UIFont.boldSystemFont(ofSize: 0)],
        for: .selected)
      viewControllers.append(viewController)
      //      viewControllers.append(viewController)
    }
    
    self.viewControllers = viewControllers
  }
  
  @objc func userLoggedout() {
    selectedIndex = 0
  }
  
  private func setupTabBar() {
    tabBar.barTintColor = UIColor.white
    tabBar.tintColor = UIColor.black
    delegate = self
  }
}

extension HomeTabBarViewController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    
    return true
  }
}
