//
//  DetailImageViewController.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 18/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class DetailImageViewController: UIViewController {
  @IBOutlet private weak var collection: UICollectionView!
  @IBOutlet private weak var pager: UIPageControl!
  
  var images: [UIImage]
  
  init(images: [UIImage]) {
    self.images = images
    super.init(nibName: "DetailImageViewController", bundle: nil)
    hidesBottomBarWhenPushed = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  private func setupUI() {
    title = "My Apps"
    collection.dataSource = self
    collection.delegate = self
    collection.register(UINib(nibName: "ImageDetailCell", bundle: nil), forCellWithReuseIdentifier: "ImageDetailCell")
  }
  
}

extension DetailImageViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return collectionView.frame.size
  }
}

extension DetailImageViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageDetailCell", for: indexPath) as! ImageDetailCell
    cell.item = images[indexPath.row]
    
    return cell
  }
  
  
}

extension DetailImageViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let page = Int(ceil(scrollView.contentOffset.x / scrollView.frame.size.width))
    pager.currentPage = page
  }
}
