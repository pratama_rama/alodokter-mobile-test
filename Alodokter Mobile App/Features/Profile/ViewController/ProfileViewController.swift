//
//  ProfileViewController.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
  @IBOutlet private weak var txtEmail: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  private func setupUI() {
    navigationItem.title = "My Apps"
    txtEmail.text = Persistent.shared.get(key: .email)
  }
  
  @IBAction func logoutTapped(_ sender: Any) {
    Persistent.shared.set(key: .isLogin, value: "0")
    tabBarController?.selectedIndex = 0
    AppDelegate.current?.window?.rootViewController = AppDelegate.current?.loginViewController
  }
}

