//
//  LoginViewController.swift
//  Alodokter Mobile App
//
//  Created by Pratama Ramadhan on 17/09/19.
//  Copyright © 2019 Alodokter. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
  @IBOutlet private weak var scrollView: UIScrollView!
  @IBOutlet private weak var txtPassword: UITextField!
  @IBOutlet private weak var txtEmail: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
  }
  
  private func setupUI() {
    title = "Login"
    scrollView.keyboardDismissMode = .interactive
  }
  
  @IBAction func loginTapped(_ sender: Any) {
    let input = [
      ValidationInput(type: [.notEmpty, .email], name: "Email", control: txtEmail),
      ValidationInput(type: [.notEmpty], name: "Password", control: txtPassword)
    ]
    
    switch Validation.validate(inputs: input) {
    case .valid:
      login()
    case .invalid(let message, let field):
      UIAlertController.showAlert(message: message,
                                  buttonTitle: "OK",
                                  dismissalHandler: { _ in field?.becomeFirstResponder() })
    }
  }
  
  private func login() {
    Persistent.shared.set(key: .isLogin, value: "1")
    Persistent.shared.set(key: .email, value: txtEmail.text!)
    txtEmail.text = ""
    txtPassword.text = ""
    AppDelegate.current?.window?.rootViewController = AppDelegate.current?.homeViewController
  }
}

